import { combineReducers } from 'redux'
import {
    SHOW_LOADING,
    HIDE_LOADING,
    TOGGLE_ALERT,
    AlertTypes
} from '../Actions'

function loading(state = {isLoading:false,loadingText:'کمی صبر کنید'}, action) {
  switch (action.type) {
    case SHOW_LOADING:
      return {
          isLoading:true,
          loadingText:action.text
        }
    case HIDE_LOADING:
      return {
        isLoading:false,
        loadingText:'کمی صبر کنید'
      }
    default:
      return state
  }
}

function alert(state = {alertText: 'خطایی رخ داده'}, action) {
  let alertTime = 5000;
    switch (action.type) {
      case TOGGLE_ALERT:
        if(action.alertType === AlertTypes.ERROR) {
          
          document.getElementById('red-alert-box').classList.add('alert-box-visible');
          setTimeout(()=>{document.getElementById('red-alert-box').classList.remove('alert-box-visible');},alertTime);
          
        } else if(action.alertType === AlertTypes.SUCCESS){

          document.getElementById('green-alert-box').classList.add('alert-box-visible');
          setTimeout(()=>{document.getElementById('green-alert-box').classList.remove('alert-box-visible');},alertTime);

        } else if(action.alertType === AlertTypes.WARNING){
          document.getElementById('orange-alert-box').classList.add('alert-box-visible');
          setTimeout(()=>{document.getElementById('orange-alert-box').classList.remove('alert-box-visible');},alertTime);
        }
        return {
          alertText: action.alertText
        }
      default:
        return state
    }
} 


const parknersApp = combineReducers({
    loading,
    alert
})

export default parknersApp