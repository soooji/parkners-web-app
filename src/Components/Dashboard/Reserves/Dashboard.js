import React, { Component } from 'react'

import { connect } from 'react-redux'
import {showLoading,hideLoading,toggleAlert,AlertTypes} from '../../../Actions'

import { BASE_URL, SETTOKEN, USER_ROLE, CATCH_HANDLER, TRY_CATCH_HANDLER} from '../../../Statics/functions';
import axios from 'axios'
import UserReserveItem from './UserReserveItem'

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
        cars:[],

        hasNoReserveToday: false,
        hasNoReserveFuture: false,

        todayParkEvents: [],
        futureParkEvents: []
    }
}
componentDidMount() {
    this.getUserParkEvents(true);
    this.getUserParkEvents(false);
    this.getCars();
    // setTimeout(()=>{
    //     this.props.showAlert('خطا در دریافت',AlertTypes.ERROR);
    // },4000)
}
getUserParkEvents = async (isToday) => {
    try {
        this.props.showLoading('در حال دریافت رزرو های شما');
        this.setState({hasNoReserveToday: false,hasNoReserveFuture: false});
        this.setState({currentActiveTab:isToday ? 1 : 2});
        const token = await localStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'GET',
            url: `${BASE_URL}park_events/${isToday ? 'today' : 'future'}/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
        };
        axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            if(isToday) {
                this.setState({todayParkEvents : r.data.park_events})
                if(r.data.park_events.length == 0) {
                    this.setState({hasNoReserveToday: true});
                }
            } else {
                this.setState({futureParkEvents : r.data.park_events})
                if(r.data.park_events.length == 0) {
                    this.setState({hasNoReserveFuture: true});
                }
            }
        })
        .then(()=>{
            this.props.hideLoading();
        })
        .catch(e=>{
            this.props.hideLoading();
            CATCH_HANDLER(e);
        })
    } catch (error) {
        this.props.hideLoading();
        TRY_CATCH_HANDLER(error);
    }
}
getCars = async () => {
    try {
        const token = await localStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'GET',
            url: `${BASE_URL}cars/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
        };
    axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({cars:r.data.cars});
        })
        .catch(e=>{
            CATCH_HANDLER(e);
        })
    } catch (error) {
        TRY_CATCH_HANDLER(error);
    }
  }
toggleReserving() {
    this.props.loading.isLoading ? this.props.hideLoading() : this.props.showLoading('کمی صبر کنید')
}
  render() {
    return (
      <div className="dashboard">
        <div>
            {this.state.todayParkEvents.map(
                (v,k)=>
                <UserReserveItem
                    showAlert={(text,type)=>this.props.showAlert(text,type)}
                    showLoading={(text)=>this.props.showLoading(text)}
                    hideLoading={()=>this.props.hideLoading()}

                    getParkingList={()=>this.getUserParkEvents(true)}
                    key={k}
                    parkEventId={v.id}
                    setCurrentReserve={(has,id)=>this.props.setCurrentReserve(has,id)}
                    toggleReserving={()=>this.toggleReserving()}
                    flagTitle={v.park_slot.title}
                    parkingName={v.parking_type.place.title} 
                    desc={v.parking_type.description}
                    carId={v.car.id}
                    isEndless={v.is_endless}
                    carName={v.car.given_name}
                    startTime={v.start_time}
                    endTime={v.end_time}
                    cars={this.state.cars}
                    enterIsVisible={true}
                />  
            )}
            {this.state.futureParkEvents.map(
                (v,k)=>
                <UserReserveItem
                    showAlert={(text,type)=>this.props.showAlert(text,type)}
                    showLoading={(text)=>this.props.showLoading(text)}
                    hideLoading={()=>this.props.hideLoading()}

                    getParkingList={()=>this.getUserParkEvents(false)}
                    key={k}
                    parkEventId={v.id}
                    setCurrentReserve={(has,id)=>this.props.setCurrentReserve(has,id)}
                    toggleReserving={()=>this.toggleReserving()}
                    flagTitle={v.park_slot.title}
                    parkingName={v.parking_type.place.title} 
                    desc={v.parking_type.description}
                    carId={v.car.id}
                    isEndless={v.is_endless}
                    carName={v.car.given_name}
                    startTime={v.start_time}
                    endTime={v.end_time}
                    cars={this.state.cars}
                    enterIsVisible={true}
                />  
            )}
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
    return {loading: state.loading}
}
const mapDispatchToProps = (dispatch) => {
    return {
        showLoading: (text) => {
            dispatch(showLoading(text))
        },
        hideLoading: ()=>{
            dispatch(hideLoading())
        },
        showAlert:(text,alertType)=>{
            dispatch(toggleAlert(text,alertType));
        }
    }
}
const ConnectedDashboard = connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard)
  
export default ConnectedDashboard