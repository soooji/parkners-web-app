import React, { Component } from 'react'
import { BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE,CATCH_HANDLER, TRY_CATCH_HANDLER} from '../../../Statics/functions';
import axios from 'axios'
import { AlertTypes } from '../../../Actions';
export default class UserReserveItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,

      isVisibleEdit: false,

      selectedCarName: '',
      selectedCarId: 0,

      isTimePickerVisible: false,
      isStartTime: true,
      startTimeHours: '',
      startTimeMiuntes: '',
      endTimeHours: '',
      endTimeMiuntes: '',
      startTimeObject: new Date(),
      endTimeObject: new Date(),

      isVisibleResultModal: false,
      isVisibleCarModal: false,
      isVisibleEnterConfirmModal: false,
      isVisibleCancelModal: false
    }
  }
  componentDidMount() {
    this.setState({selectedCarId: this.props.carId});
    this.createDateObject();
  }
  enterParking = async () => {
    try {
      const token = await localStorage.getItem('token');
      const options = {
          method: 'GET',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/enter/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      this.props.showLoading('در حال ورود به پارکینگ');
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
      })
      .then(()=>{
        this.setState({isVisibleEnterConfirmModal:false});
        this.props.setCurrentReserve(true,this.props.parkEventId);
        this.props.hideLoading();
      })
      .then(()=>{
        // setTimeout(()=>this.props.setCurrentBase("current-reserve"),500);
        //TODO: route
      })
      .catch(e=>{
        this.props.hideLoading();
        CATCH_HANDLER(e);
      })
    } catch (error) {
        this.props.hideLoading();
        TRY_CATCH_HANDLER(error);
    }
  }
  putReserve = async (carId) => {
    try {
      const token = await localStorage.getItem('token');
      const options = {
          method: 'PUT',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          data : {
            park_event : {
              car_id : carId,
              start_datetime: this.props.startTime,
              end_datetime : this.props.endTime
            }
          }
      };
      this.props.showLoading('در حال اعمال تغییرات');
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({selectedCarId:carId});
      })
      .then(r=>{
        this.props.hideLoading();
        this.props.showAlert('خودرو با موفقیت تغییر کرد',AlertTypes.SUCCESS);
        this.setState({isVisibleEdit: false});
        this.props.getParkingList();
      })
      .catch(e=>{
          this.props.hideLoading();
          CATCH_HANDLER(e);
      })
    } catch (error) {
        this.props.hideLoading();
        TRY_CATCH_HANDLER(error);
    }
  }
  deleteReserve = async () => {
    try {
      const token = await localStorage.getItem('token');
      this.setState({isVisibleCancelModal:false});
      const options = {
          method: 'DELETE',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      this.props.showLoading('در حال حذف رزرو');
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.props.hideLoading();
      })
      .then(r=>{
        this.props.getParkingList();
      })
      .catch(e=>{
          this.props.hideLoading();
          CATCH_HANDLER(e);
      })
    } catch (error) {
        TRY_CATCH_HANDLER(error);
    }
  }
  toggleActions() {
    this.setState({isOpen:!this.state.isOpen})
  }
  createDateObject() {
    let startTime = new Date();
    startTime.setHours(this.props.startTime.hour,this.props.startTime.minute);
    this.setState({startTimeObject:startTime});

    let endTime = new Date();
    endTime.setHours(this.props.endTime.hour,this.props.endTime.minute);
    this.setState({endTimeObject:endTime});
  }
  render() {
    return (
        <div className="reserve-item" onClick={()=>!this.state.isOpen ? this.toggleActions() : null}>
        <div className="flag">
            {this.props.flagTitle}
        </div>
        <div className={`arrow ${this.state.isOpen ? 'open' : ''}`} onClick={()=>this.toggleActions()}>
            <i className="icon-top"/>
        </div>
        <div>
            <div className="info-item">
                <i className="icon-parking"/>
                <div className="text">{this.props.parkingName}</div>
            </div>
            <div className="info-item">
                <i className="icon-calendar"/>
                <div className="text">
                {`${this.props.startTime.year}/${this.props.startTime.month}/${this.props.startTime.day}`}
                </div>
            </div>
            <div className="info-item">
                <i className="icon-clock"/>
                <div className="text">
                    {this.props.startTime ? this.props.startTime.hour ?
                    `از ${this.props.startTime.hour}:${this.props.startTime.minute == 0 ? '00' : this.props.startTime.minute}` : "" : ""}
                    {this.props.endTime ? this.props.endTime.hour ?
                    ` ${this.props.isEndless ? "حداکثر تا" : "تا"} ${this.props.endTime.hour}:${this.props.endTime.minute == 0 ? '00' : this.props.endTime.minute}` : "" : ""}
                </div>
            </div>
            <div className="info-item">
                <i className="icon-info"/>
                <div className="text">{this.props.desc}</div>
            </div>
        </div>
        {this.state.isOpen ?
            <div className="action-box">
              <div className="action-title">
               تغییر خودرو رزرو :
              </div>
              <select onChange={(e)=>this.putReserve(e.target.value)} value={this.state.selectedCarId}>
                {this.props.cars.map(
                  (v,k)=>
                  <option key={k} value={v.id}>{v.given_name}</option>
                )}
              </select>
              <div onClick={()=>this.deleteReserve()} className="action-button red-button">
                لغو رزرو
              </div>
              <div className="action-button green-button">
                ورود به پارکینگ
              </div>
            </div> : null}
      </div>
    )
  }
}