import React, { Component } from 'react'
import { Redirect } from "react-router-dom";

//redux
import { connect } from 'react-redux'
import {showLoading,hideLoading} from '../../Actions'
class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: true
        }  
    }

    componentDidMount() {
        this.props.showLoading('کمی صبر کنید');
        const token = localStorage.getItem('token');
        console.log(token);
        this.props.hideLoading();
        this.setState({isLoggedIn:token ? true : false});
    }
  render() {
    return (
      <div>
        Home Page
        {!this.state.isLoggedIn ?
            <Redirect to={{pathname: "/auth"}}/> :
            <Redirect to={{pathname: "/dashboard/reserves"}}/>
        }
      </div>
    )
  }
}


const mapStateToProps = (state) => {
    return {loading: state.loading}
}
const mapDispatchToProps = (dispatch) => {
    return {
        showLoading: (text) => {
            dispatch(showLoading(text))
        },
        hideLoading: ()=>{
            dispatch(hideLoading())
        }
    }
}
const ConnectedSplashScreen = connect(
    mapStateToProps,
    mapDispatchToProps
)(SplashScreen)
  
export default ConnectedSplashScreen