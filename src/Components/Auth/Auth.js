import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import {showLoading,hideLoading} from '../../Actions'

import { BASE_URL, SETTOKEN, USER_ROLE, CATCH_HANDLER, TRY_CATCH_HANDLER } from '../../Statics/functions';
class AuthComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn : false,
            authState:'phone', //code register phone
            phone: '',
            firstName:'',
            lastName:'',
            code:'',
            introducer: '',

            counter: 0,
        }
    }
    componentDidMount() {
    }
    sendPhone = async()=> {
        if(this.state.phone.length == 0) {
            this.toggleAlert(true,this.props.lang.Login.enterPhone);
            return
        }
        const options = {
            method: 'GET',
            url: `${BASE_URL}auth/check_phone/${this.state.phone}/`,
            headers: { 'content-type': 'application/json','role':USER_ROLE},
        };
        this.props.showLoading('لطفا کمی صبر کنید');
        axios(options)
        .then(()=>{
            this.setState({authState:'code'});
            this.countItDown(60);
        })
        .then(()=>{
            this.props.hideLoading();
        })
        .catch(e=>{
            this.props.hideLoading();
            CATCH_HANDLER(e);
        })
    }
    countItDown(time) {
        this.setState({counter: time});
        setInterval(()=>{
            if(this.state.counter>0) {
                this.setState({counter: this.state.counter - 1});
            } else {
                return;
            }
        },1000);
    }
    checkCode = async()=> {
        this.props.showLoading('لطفا کمی صبر کنید');
        const options = {
            method: 'GET',
            headers: {'content-type': 'application/json','role':USER_ROLE},
            url: `${BASE_URL}auth/check_code/${this.state.phone}/${this.state.code}/`,
        };
        axios(options)
        .then(r=>{
            this.props.hideLoading();
            if(r.status == 201) {
                localStorage.setItem('token', r.data.token);
                localStorage.setItem('phone', r.data.user.phone.toString());
                this.setState({authState:'register'});
            } else {
                localStorage.setItem('token', r.data.token);
                localStorage.setItem('first_name', r.data.user.first_name);
                localStorage.setItem('last_name', r.data.user.last_name);
                localStorage.setItem('phone', r.data.user.phone.toString());
                //END NOTIF
                this.setState({isLoggedIn : true});
            }
        })
        .catch(e=>{
            this.props.hideLoading();
            CATCH_HANDLER(e);
        })
    }
    register = async()=> {
        try {
            const token = await localStorage.getItem('token');   
            this.props.showLoading('در حال ایجاد حساب کاربری');
            const options = {
                method: 'POST',
                headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
                data: JSON.stringify({
                    'profile': {
                        'user': {
                            'first_name': this.state.firstName,
                            'last_name': this.state.lastName
                        },                        
                        'recommended_by': this.state.introducer
                    }
                }),
                url: `${BASE_URL}users/profile/`,
            };
            axios(options)
            .then(r=>{
                localStorage.setItem('first_name', r.data.user.first_name);
                localStorage.setItem('last_name', r.data.user.last_name);
                localStorage.setItem('phone', r.data.user.phone.toString());
                SETTOKEN(r.data.token);
            })
            .then(()=>{
                this.props.hideLoading();
                this.setState({isLoggedIn : true});
            })
            .catch(e=>{
                this.props.hideLoading();
                CATCH_HANDLER(e);
            })
        } catch (error) {
            this.props.hideLoading();
            TRY_CATCH_HANDLER(error);
        }
    }
  render() {
    return (
      <div className="auth-box">
      {
          this.state.isLoggedIn ?
          <Redirect to={{pathname: "/dashboard/reserves"}}/>
          : null
      }
        <i className="icon-parkners logo"/>
        {this.state.authState == "phone" ? 
            <>
                <div className="input-title">
                تلفن همراه خود را واد کنید :
                </div>
                <input 
                onChange={(v) => this.setState({phone: v.target.value})}
                placeholder="تلفن همراه"/>
                <button onClick={()=>this.sendPhone()}>ورود</button>
            </>
        :null}
        {this.state.authState == "code" ? 
            <>
                <div className="input-title">
                کد پیامک شده را وارد کنید :
                </div>
                <input 
                onChange={(v) => this.setState({code: v.target.value})}
                placeholder="کد فعالسازی"/>
                <button onClick={()=>this.checkCode()}>تایید</button>
                <div className="input-title">
                    {this.state.counter > 0 ? `${this.state.counter} ثانیه تا امکان ارسال مجدد` : <button onClick={()=>this.sendPhone()}>ارسال مجدد کد</button>}
                </div>
            </>
        :null}
        {this.state.authState == "register" ? 
            <>
                <div className="input-title">
                ثبت نام :
                </div>
                <input 
                onChange={(v) => this.setState({firstName: v.target.value})}
                placeholder="نام"/>
                <input 
                onChange={(v) => this.setState({lastName: v.target.value})}
                placeholder="نام خانوادگی"/>
                <input 
                onChange={(v) => this.setState({introducer: v.target.value})}
                placeholder="کد معرف (اختیاری)"/>
                <button onClick={()=>this.register()}>تایید</button>
            </>
        :null}
      </div>
    )
  }
}
const mapStateToProps = (state) => {
    return {loading: state.loading}
}
const mapDispatchToProps = (dispatch) => {
    return {
        showLoading: (text) => {
            dispatch(showLoading(text))
        },
        hideLoading: ()=>{
            dispatch(hideLoading())
        }
    }
}
const Auth = connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthComponent)
  
export default Auth