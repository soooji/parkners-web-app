import React, { Component } from 'react'
//css
import '../Statics/styles.css'
import '../Statics/font-icons/css/sooji.css'
import '../Statics/font-icons/css/sooji-codes.css'
import '../Statics/font-icons/css/sooji-embedded.css'
import '../Statics/font-icons/css/sooji-ie7-codes.css'
import '../Statics/font-icons/css/sooji-ie7.css'
//redux
import { connect } from 'react-redux'
import {showLoading,hideLoading,toggleAlert} from '../Actions'
//internal
import { COLORS } from '../Statics/functions';
//packages
import Loader from 'react-loader-spinner'
import { Route } from "react-router-dom";
//components
import Header from '../Containers/Base/Header';
import Auth from './Auth/Auth';
import SplashScreen from './Auth/Splash';
import UserReserves from './Dashboard/Reserves/Dashboard'
class App extends Component {
  render() {
    return (
      <div className="body">
        <div className="alert-box" id="red-alert-box">{this.props.alert.alertText}</div>
        <div className="alert-box" id="green-alert-box">{this.props.alert.alertText}</div>
        <div className="alert-box" id="orange-alert-box">{this.props.alert.alertText}</div>
        {this.props.loading.isLoading ?
          <div className="loading">
            <Loader type="TailSpin" color={COLORS.Black} height="100" width="100"/>
            <div>{this.props.loading.loadingText}</div>
          </div>
        : null}
        <Header/>
        <main>
          <Route path="/" exact component={SplashScreen} />
          <Route path="/auth" component={Auth} />
          <Route path="/dashboard/reserves" component={UserReserves} />
        </main>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
    return {loading: state.loading,alert: state.alert}
}
const mapDispatchToProps = (dispatch) => {
    return {
        showLoading: (text) => {
          dispatch(showLoading(text));
        },
        hideLoading: ()=>{
          dispatch(hideLoading());
        },
        showAlert:(text)=>{
          dispatch(toggleAlert(text));
        }
    }
}
const ConnectedApp = connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
  
export default ConnectedApp
