//Base
export const CURRENT_VERSION = '1.0.0';

export const SUPPORT_NUMBER = "09126653006"
export const BASE_URL = "http://dev.parkners.com/api/v1/"
export const BASE_IMG_URL = "http://dev.parkners.com"

export const USER_ROLE = "user"
var
persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
export const FIX_NUMS = function (str)
{
  if(typeof str === 'string')
  {
    for(var i=0; i<10; i++)
    {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};
//Appearance
export const COLORS = {
  White: '#FFFFFF',
    Black:"#000000",
    Green: "#3ED135",
    Red: "#FF4747",
    DarkBlue: "#202042",
    Orange: "#FFB647",
    LightGrey:"#F4F4FA",
    LighterGrey: "#DEDDE3",
    LightBlue: "#06D4E2",
    LightViolet: "#8A47FF",
    LightRed: "#FF5947",
    DarkGrey:"#AEAEAE"
};
//functions
export const ISEXPIRED = (error) => {
  if(error.response) {
      if(error.response.status == 403 || error.response.status == "403") {
          return true;
      } else {
          return false;
      }
  }
}
export const SETTOKEN = token => {
  if(token != "")
      localStorage.setItem("token",token);
  return
}
export const SET_TOKEN = token => {
    if(token != "") {
        localStorage.setItem("token",token);
    }
}
export const CATCH_HANDLER = (e) => {
  console.log(e);
  if(!e.response) {alert('خطا در ارتباط با سرور');return;}
  if(ISEXPIRED(e)) {window.open('/auth','_self');return;}
  if(e.response.status === 500) alert('خطا در ارتباط با سرور');
  else alert(e.response.data.error_message);
}
export const TRY_CATCH_HANDLER = (e) => {
  console.log(e);
  window.open('/auth','_self');
}