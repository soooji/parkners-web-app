import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoApp from './Reducers'
import {initialState} from './Store'
import ConnectedApp from './Components/App'
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
const store = createStore(todoApp,initialState)

render(
  <Router>
      <Provider store={store}>
        <ConnectedApp />
      </Provider>
  </Router>,
  document.getElementById('root')
)