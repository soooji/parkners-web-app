/*
 * action types
 */
export const SHOW_LOADING = 'SHOW_LOADING'
export const HIDE_LOADING = 'HIDE_LOADING'
export const TOGGLE_ALERT = 'TOGGLE_ALERT'
/*
 * other constants
 */
export const AlertTypes = {
  ERROR: 'ERROR',
  SUCCESS: 'SUCCESS',
  WARNING: 'WARNING'
}
/*
 * action creators
 */ 
export function showLoading(text) {
    return { type: SHOW_LOADING,text}
}
export function hideLoading() {
    return { type: HIDE_LOADING }
}
export function toggleAlert(alertText,alertType) {
    // eslint-disable-next-line no-dupe-keys
    return { type: TOGGLE_ALERT, alertText,alertType }
}