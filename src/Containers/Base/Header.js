import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
        <header>
          <i className="icon-menu"/>
          <a href="http://web.parkners.com"><i className="icon-parkners logo"/></a>
        </header>
    )
  }
}
